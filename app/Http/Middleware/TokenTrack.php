<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Ramsey\Uuid\Uuid;

class TokenTrack
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('token')) {
            $token = Uuid::uuid4();

            $instance = new Token(['uuid' => $token->serialize()]);
            $instance->save();

            session()->put('token', $instance->uuid);

            $request->token = $instance;
        }

        if(session()->has('token')) {
            $instance = Token::where('uuid', '=', session()->get('token'))->with('campaigns')->first();

            // Token lost, create a new one
            if(!$instance) {
                $token = Uuid::uuid4();
                $instance = new Token(['uuid' => $token->serialize()]);
                $instance->save();
            };

            $request->token = $instance;
        }

        View::share('token', $request->token);

        return $next($request);
    }
}
