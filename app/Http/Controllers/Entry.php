<?php

namespace App\Http\Controllers;

use App\Models\Ballot;
use App\Models\Campaign;
use Illuminate\Http\Request;

class Entry extends Controller
{

    public function __construct()
    {

    }

    protected function index(Request $request)
    {
        $request->token->load('campaigns');
        $campaigns = $request->token->campaigns;

        $data = [
            'campaigns' => $campaigns,
            'error' => null
        ];

        if($request->session()->get('error') === 'ballot_not_found') {
            $data['error'] = 'ballot_not_found';
        }


        return view ('entry', $data);
    }

    /**
     * Find and Jump to Ballot
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function findBallot(Request $request)
    {
        $ballot = Ballot::query()
            ->where('ballots.slug','ilike', $request->input('code'))
            ->with('campaign')
            ->first();

        if($ballot === null) {
            $request->session()->flash('error', 'ballot_not_found');
            return redirect('/');
        }

        return redirect('/campaign/' . $ballot->campaign->slug . '/ballot/' . $ballot->slug);
    }


}
