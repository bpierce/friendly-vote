<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class CampaignsController extends Controller
{

    public function __construct()
    {

    }

    protected function index(Request $request)
    {
        $request->token->load('campaigns', 'campaigns.ballots');

        $campaigns = $request->token->campaigns;

        $campaigns->sortBy('updated_at');

        return view('campaign.index', ['campaigns'=>$campaigns]);
    }

    /**
     *
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function store(Request $request)
    {
        $campaign = new Campaign(['name' => $request->input('name'), 'token_id' => $request->token->id]);

        $campaign->save(); // Commits to the database;

        return redirect('/campaign/' . $campaign->slug);
    }

    /**
     * Get Campaign
     *
     * @param Campaign $campaign
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getCampaign(Campaign $campaign, Request $request)
    {
        $campaign->load('ballots', 'ballots.candidates');

        return view('campaign.ballot_list', ['campaign'=>$campaign]);
    }

}
