<?php

namespace App\Http\Controllers;

use App\Models\Ballot;
use App\Models\Campaign;
use App\Models\Candidate;
use App\Models\Vote;

use App\Services\SimpleVoteService;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Ramsey\Uuid\Uuid;

class BallotController extends Controller
{
    private $simpleBallotService;

    /**
     * BallotController constructor.
     *
     * @param SimpleVoteService $simpleBallotService
     */
    public function __construct(SimpleVoteService $simpleBallotService)
    {
        $this->simpleBallotService = $simpleBallotService;
    }

    /**
     * Create new Ballot
     *
     * @param Campaign $campaign
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function store(Campaign $campaign, Request $request)
    {
        $options = [];

        if($request->input('type') === 'approvalVote') {
            $options['approval_token_max_votes'] = $request->input('approval_token_max_votes');
            $options['approval_candidate_max_votes'] = $request->input('approval_candidate_max_votes');
        }

        $ballot = new Ballot([
            'name'=>$request->input('name'),
            'type' => $request->input('type'),
            'type_settings' => $options,
            'token_id' => $request->token->id,
            'campaign_id' => $campaign->id,
            'polls_open' => false,
            'write_in' => false,
            'complete' => false
        ]);

        $ballot->save();

        return redirect('/campaign/' . $campaign->slug . '/ballot/' . $ballot->slug);
    }


    /**
     * Get Ballot
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function getBallot(Campaign $campaign, Ballot $ballot, Request $request)
    {
        $ballot->load('votes', 'votes.token', 'candidates', 'voter_roll', 'voter_roll.token');

        $hasMyVote = boolval(Vote::where('token_id', '=', $request->token->id)->where('ballot_id', '=', $ballot->id)->distinct('token_id')->select('token_id')->count());

        $results = new Collection();

        // Tabulate Results
        if($ballot->complete) {
            switch($ballot->type) {
                case 'simpleVote':
                case 'approvalVote':
                    $results = $this->simpleBallotService->tallyResults($ballot);
                    break;
            }
        }

        return view('ballot.ballot_home', ['ballot'=> $ballot, 'campaign' => $campaign, 'hasMyVote'=>$hasMyVote, 'results' => $results]);
    }

    /**
     * Get Ballot
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected function jsonGetBallot(Campaign $campaign, Ballot $ballot, Request $request)
    {
        $ballot->load('votes', 'votes.token', 'candidates');

        $hasMyVote = boolval(Vote::where('token_id', '=', $request->token->id)->where('ballot_id', '=', $ballot->id)->distinct('token_id')->select('token_id')->count());

        return response()->json(['ballot'=> $ballot, 'campaign' => $campaign]);
    }

    /**
     * Add Candidate to Ballot
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    protected function createCandidate(Campaign $campaign, Ballot $ballot, Request $request)
    {
        $candidate = new Candidate([
            'ballot_id' => $ballot->id,
            'token_id' => $request->token->id,
            'uuid' => Uuid::uuid4()->serialize(),
            'name' => $request->input('name')
        ]);

        $candidate->save();

        return redirect('/campaign/' . $campaign->slug . '/ballot/' . $ballot->slug);
    }

    /**
     * API Add Candidate to Ballot
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @throws \Exception
     */
    protected function apiCreateCandidate(Campaign $campaign, Ballot $ballot, Request $request)
    {
        $candidate = new Candidate([
            'ballot_id' => $ballot->id,
            'token_id' => $request->token->id,
            'uuid' => Uuid::uuid4()->serialize(),
            'name' => $request->input('name')
        ]);

        $candidate->save();

        return response()->json($candidate);
    }

    /**
     * Toggle Polls Open flag
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function togglePollsOpen(Campaign $campaign, Ballot $ballot, Request $request)
    {
        if($ballot->token_id !== $request->token->id) abort(403, 'Unauthorized.');

        $ballot->polls_open = !$ballot->polls_open;

        $ballot->save();

        return redirect('/campaign/'.$campaign->slug.'/ballot/' . $ballot->slug);
    }

    /**
     * Toggle Write In Candidates flag
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function toggleWriteIn(Campaign $campaign, Ballot $ballot, Request $request)
    {
        if($ballot->token_id !== $request->token->id) abort(403, 'Unauthorized.');

        $ballot->write_in = !$ballot->write_in;

        $ballot->save();

        return redirect('/campaign/'.$campaign->slug.'/ballot/' . $ballot->slug);
    }


    /**
     * Cast Ballot
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    protected function storeVote(Campaign $campaign, Ballot $ballot, Request $request)
    {
        $candidate = Candidate::where('uuid', '=', $request->input('candidate'))->first();

        switch($ballot->type) {
            case 'simpleVote':
                // Check for past votes
                $vote = $this->simpleBallotService->buildSimpleVote($ballot, $candidate, $request->token, $request->input('name'), $request->input('time_at_poll'));
                break;
            case 'approvalVote':

                $vote = $this->simpleBallotService->buildApprovalVote($ballot, $request->input('votes'), $request->token, $request->input('name'), $request->input('time_at_poll'));
                break;
        }

        if($request->acceptsJson()) return response()->json($vote);
        if($request->acceptsHtml()) return redirect('/campaign/'.$campaign->slug.'/ballot/'.$ballot->slug);
    }

    /**
     * Closing voting and show results!
     *
     * @param Campaign $campaign
     * @param Ballot $ballot
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function tallyBallot(Campaign $campaign, Ballot $ballot, Request $request)
    {

        if($ballot->token_id !== $request->token->id)  abort(403, 'Only ballot creators can tally the vote');

        $ballot->complete = true;
        $ballot->save();

        return redirect('/campaign/' . $campaign->slug . '/ballot/' . $ballot->slug);
    }
}
