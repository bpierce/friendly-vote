<?php

/**
 * Generate URL Slug
 *
 * @param $length
 *
 * @return string
 */
function generateSlug($length) {

    $chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ";

    $slug = "";

    while(strlen($slug) < $length) {
        $slug .= substr($chars, rand(0,strlen($chars)-1), 1);
    }

    return $slug;
}


function voteTypeToText($type) {
    switch($type) {
        case 'simpleVote':
            return 'Simple Vote';
            break;
        case 'approvalVote':
            return 'Approval Vote';
            break;
        case 'pointVote':
            return 'Point Vote';
            break;
        case 'scaledVote':
            return 'Scaled Vote';
            break;
        case 'preferentialVote':
            return 'Preferential Vote';
            break;
    }
}
