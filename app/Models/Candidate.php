<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Candidates
 *
 * @package App\Models
 */
class Candidate extends Model
{
    protected $table = 'candidates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token_id', 'ballot_id', 'campaign_id', 'uuid', 'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'ballot_id',
        'campaign_id'
    ];

    protected $casts = [];


    /**
     * Parent Campaign relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Ballot Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ballot()
    {
        return $this->belongsTo(Ballot::class);
    }



}
