<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ballot
 *
 * @package App\Models
 */
class Vote extends Model
{
    protected $table = 'votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ordinal', 'value', 'type', 'token_id', 'ballot_id', 'campaign_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'token_id', 'ballot_id', 'campaign_id'
    ];

    protected $casts = [];

    /**
     * Parent Ballot relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ballot()
    {
        return $this->belongsTo(Ballot::class);
    }

    /**
     * Parent Token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function token()
    {
        return $this->belongsTo(Token::class);
    }

    /**
     * Candidate relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function candidate()
    {
        return $this->belongsTo(Candidate::class);
    }

}
