<?php namespace App\Models;

use App\Services\SimpleVoteService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * Class Ballot
 *
 * @package App\Models
 */
class Ballot extends Model
{
    protected $table = 'ballots';

    protected $request;

    /**
     * Ballot constructor
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->request = app()->make(Request::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'type', 'token_id', 'type_settings', 'campaign_id', 'polls_open', 'write_in', 'complete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    /**
     * Cast attributes as type
     *
     * @var array
     */
    protected $casts = [
        'type_settings' => 'array'
    ];

    /**
     * Extension Attributes
     *
     * @var array
     */
    protected $appends = [
        'hasVoted'
    ];

    /**
     * Get Implicit Routing Binding Column
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Parent Campaign relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Candidates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    /**
     * Votes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votes()
    {
        return $this->hasMany(Vote::class);
    }

    /**
     * Voter Roll
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function voter_roll()
    {
        return $this->hasMany(VoterRoll::class);
    }

    /**
     * Determine if user has voted
     *
     * @return bool
     */
    public function getHasVotedAttribute()
    {
        $check = VoterRoll::where(['token_id' => $this->request->token->id, 'ballot_id' => $this->id])->first();

        return ($check !== null);
    }

}

/**
 * Hook pre-creation of ballot to generate URL slug
 *
 */
Ballot::creating(function(Ballot $ballot) {

    $slug = generateSlug(4);
    $count = 0;

    while(Ballot::where('slug', '=', $slug)->get()->count() > 0) {
        $slug = generateSlug(4);
        $count++;

        if($count > 10) throw new \Exception('Ballot Slug namespace exhausted');
    }

    $ballot->setAttribute('slug', $slug);

    return $ballot;
});
