<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ballot
 *
 * @package App\Models
 */
class VoterRoll extends Model
{
    protected $table = 'voter_rolls';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ballot_id', 'token_id', 'time_at_poll'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'token_id', 'ballot_id'
    ];

    protected $casts = [];

    /**
     * Parent Ballot relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ballot()
    {
        return $this->belongsTo(Ballot::class);
    }

    /**
     * Parent Token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function token()
    {
        return $this->belongsTo(Token::class);
    }

}
