<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Campaign
 *
 * @package App\Models
 */
class Campaign extends Model
{
    protected $table = 'campaigns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'slug', 'name', 'day', 'token_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    /**
     * Get Implicit Routing Binding Column
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Ballot relationships
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ballots()
    {
        return $this->hasMany(Ballot::class);
    }

}

/**
 * Hook pre-creation of campaign to generate URL slug
 *
 */
Campaign::creating(function(Campaign $campaign) {

    $slug = generateSlug(4);
    $count = 0;

    while(Campaign::where('slug', '=', $slug)->get()->count() > 0) {
        $slug = generateSlug(4);
        $count++;

        if($count > 10) throw new \Exception('Campaign Slug namespace exhausted');
    }

    $campaign->setAttribute('slug', $slug);

    return $campaign;
});
