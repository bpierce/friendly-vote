<?php namespace App\Services;

use App\Models\Ballot;
use App\Models\Candidate;
use App\Models\Token;
use App\Models\Vote;
use App\Models\VoterRoll;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

/**
 * Class SimpleVoteService
 *
 * @package App\Services
 */
class SimpleVoteService
{

    private $model;

    /**
     * SimpleVoteService constructor.
     *
     * @param Vote $model
     */
    public function __construct(Vote $model)
    {
        $this->model = $model;
    }


    /**
     * Build Simple Vote
     *
     * @param Ballot $ballot
     * @param Candidate $candidate
     * @param Token $token
     * @param string $name
     * @param integer $time_at_poll
     *
     * @return Vote
     * @throws \Exception
     */
    public function buildSimpleVote(Ballot $ballot, Candidate $candidate, Token $token, $name, $time_at_poll)
    {

        DB::beginTransaction();

        try {

            if (!$this->checkVoterRoll($ballot, $token)) {
                abort(400, 'Already voted');
            }

            $vote = new $this->model();

            $vote->ballot()->associate($ballot);
            $vote->candidate()->associate($candidate);
            $vote->token()->associate($token);
            $vote->uuid = Uuid::uuid4()->serialize();
            $vote->type = 'simple';

            $vote->save();

            // Update token's saved name
            $token->name = $name ?? 'Anonymous';
            $token->save();

            // Create entry on voter roll
            VoterRoll::create(['ballot_id' => $ballot->id, 'token_id' => $token->id, 'time_at_poll' => $time_at_poll ?? 0]);

            DB::commit();
        }

        catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return $vote;
    }

    /**
     * Create Approval Vote
     *
     * @param Ballot $ballot
     * @param array $votes
     * @param Token $token
     * @param $name
     * @param $time_at_poll
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function buildApprovalVote(Ballot $ballot, array $votes, Token $token, $name, $time_at_poll)
    {
        DB::beginTransaction();

        try {
            if (!$this->checkVoterRoll($ballot, $token)) {
                abort(400, 'Already voted');
            }

            foreach($votes as $vote) {

                if($vote['count'] === 0) continue;

                $candidate = Candidate::where('uuid', '=', $vote['candidate'])->firstOrFail();

                $i = 0;
                $target = $vote['count'];

                while($i < $target) {
                    $vote = new $this->model();

                    $vote->ballot()->associate($ballot);
                    $vote->candidate()->associate($candidate);
                    $vote->token()->associate($token);
                    $vote->uuid = Uuid::uuid4()->serialize();
                    $vote->type = 'approval';

                    $vote->save();

                    $i++;
                }

            }

            // Update token's saved name
            $token->name = $name ?? 'Anonymous';
            $token->save();

            // Create entry on voter roll
            VoterRoll::create(['ballot_id' => $ballot->id, 'token_id' => $token->id, 'time_at_poll' => $time_at_poll ?? 0]);

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        return response()->json(['success' => true]);
    }

    /**
     * Check Voter Roll
     *
     * Has the token cast a vote on this ballot before?
     *
     * @param Ballot $ballot
     * @param Token $token
     *
     * @return boolean
     */
    public function checkVoterRoll(Ballot $ballot, Token $token)
    {
        $lookup = Vote::where([
            'ballot_id' => $ballot->id,
            'token_id' => $token->id
        ])->first();

        return (!$lookup);
    }

    /**
     * Tally Results
     *
     * @param Ballot $ballot
     *
     * @return mixed
     */
    public function tallyResults(Ballot $ballot)
    {
        $totals = Candidate::
            selectRaw('candidates.name, candidates.uuid, COUNT(DISTINCT votes.id) as total_votes')
            ->leftJoin('votes', 'candidates.id', '=', 'votes.candidate_id')
            ->where('candidates.ballot_id', $ballot->id)
            ->groupBy('candidates.name', 'candidates.uuid')
            ->orderBy('total_votes', 'DESC')
            ->get();

        return $totals;
    }

}
