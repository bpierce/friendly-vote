@include('common.header')

<body style="height: 100%">
    <div id="fv-app" class="entry-wrap text-center h-100">
        <div class="entry">
            <div style="font-size: 72px" class="fas fa-comments mb-5"></div>

            <h1 class="h3 mb-3 font-weight-normal">Friendly.Vote</h1>

            <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-join-tab" data-toggle="pill" href="#pills-join" role="tab" aria-controls="pills-join" aria-selected="true">Join</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-create-tab" data-toggle="pill" href="#pills-create" role="tab" aria-controls="pills-create" aria-selected="false">Create</a>
                </li>
                @if($campaigns->count() > 0)
                    <li class="nav-item">
                        <a class="nav-link" id="pills-campaigns-tab" data-toggle="pill" href="#pills-campaigns" role="tab" aria-controls="pills-campaigns" aria-selected="false">My Sessions</a>
                    </li>
                @endif
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-join" role="tabpanel" aria-labelledby="pills-join-tab">
                    <form method="post" action="/jump">
                    @csrf
                    @if($error === 'ballot_not_found')
                        <div class="alert alert-warning">Unable to find the vote.</div>
                    @endif
                    <div class="form-group">
                        <label for="code" class="sr-only">Campaign Code</label>
                        <input type="text" name="code" id="code" class="form-control text-uppercase" maxlength="4" placeholder="Four Letter Code" required autofocus>
                    </div>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Go</button>
                    </form>
                </div>
                <div class="tab-pane fade" id="pills-create" role="tabpanel" aria-labelledby="pills-create-tab">
                    <form method="post" action="/campaign">
                        @csrf
                        <div class="form-group">
                            <label for="session_name" class="sr-only">Session Name</label>
                            <input type="text" id="session_name" name="name" class="form-control" maxlength="35" placeholder="Session Name" required autofocus>
                        </div>

                        <button class="btn btn-lg btn-primary btn-block" type="submit">Get Started</button>
                    </form>
                </div>
                @if($campaigns->count() > 0)
                    <div class="tab-pane fade text-left" id="pills-campaigns" role="tabpanel" aria-labelledby="pills-campaigns-tab">
                        <div class="list-group">
                            @foreach($campaigns as $campaign)
                                <a class="list-group-item d-flex justify-content-between align-items-center" href="/campaign/{{$campaign->slug}}">
                                    {{$campaign->name}}
                                    <div data-toggle="tooltip" data-placement="bottom" title="Number of votes in session"><span class="badge badge-primary badge-pill">0</span></div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

            <p class="mt-5 mb-3 text-muted">&copy; 2018 VeDon Collaborations</p>
        </div>
    </div>
    <!-- Application core JS-->
    <script type="text/javascript" src="{{mix('/js/app.js')}}" ></script>
</body>
</html>
