@include('common.header')

<body>
    <div id="fv-app" class="wrap">
        <div class="row px-0 w-100">
            <div id="header" class="container px-5" style="max-width: 1140px;">
                <h2>Friendly.Vote</h2>
            </div>
        </div>

        <main role="main">
            @yield('content')
        </main>

        <div class="push"></div>
    </div>
    @include('common.footer')

    <!-- Application core JS-->
    <script type="text/javascript" src="{{mix('/js/app.js')}}" ></script>
</body>
