<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="VeDon Joint Development">
    <link rel="icon" href="../../../../favicon.ico">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Friendly.vote</title>

    <!-- Application core CSS -->
    <link href="{{mix('/css/app.css')}}" rel="stylesheet">
</head>
