@extends('common.template')

@section('content')

    <div class="w-100 container mt-3">

        <ul class="breadcrumb ml-0">
            <li class="breadcrumb-item"><a href="/">Home</a> </li>
            <li class="breadcrumb-item"><a href="/campaign">Sessions</a></li>
            <li class="breadcrumb-item"><a href="/campaign/{{$campaign->slug}}">{{$campaign->name}}</a></li>
            <li class="breadcrumb-item">{{$ballot->name}}</li>
        </ul>


        <div class="row justify-content-lg-between">

            <div class="col-lg-8 col-12 mb-3">

                <div class="p-3 bg-white rounded shadow-sm">

                    <!-- Ballot Header -->
                    <div class="row justify-content-between px-3">
                        <h5>{{$ballot->name}} - Motion</h5>
                        @if($token->id === $ballot->token_id && !$ballot->complete) <a href="/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}/tally" class="btn-sm btn btn-primary float-right ml-2 mb-2"><span class="fas fa-calculator mr-1"></span> Tally Results</a>@endif
                    </div>

                    <hr class="mb-2 mt-1" />

                    <!--  Ballot Type Description -->
                    <p class="text-muted font-italic">
                        This motion uses a <span class="font-weight-bold">{{voteTypeToText($ballot->type)}}</span> response method.
                        @if($ballot->type === 'approvalVote') Voters are allowed to cast a maximum of <span class="font-weight-bold">{{$ballot->type_settings['approval_token_max_votes'] ?? sizeof($ballot->candidates)}}</span>, with a limit of <span class="font-weight-bold">{{$ballot->type_settings['approval_candidate_max_votes'] ?? sizeof($ballot->candidates)}}</span> per option. @endif
                    </p>

                    <!-- Include Voting Ballot Vue Component if Polls Open -->
                    @if($ballot->polls_open)
                        @if($ballot->type === 'simpleVote')@include('vote.simple_vote')@endif
                        @if($ballot->type === 'approvalVote')@include('vote.approval_vote')@endif

                    <!-- If polling is paused/closed for non-owners -->
                    @elseif($ballot->polls_open === false && $ballot->token_id !== $token->id)
                        <span>Polling is currently paused</span>

                    <!-- Ballot's not open for poll owner !-->
                    @elseif($ballot->polls_open === false && $ballot->token_id === $token->id)
                    <div class="list-group">

                        @foreach($ballot->candidates as $candidate)
                            <div class="list-group-item d-flex justify-content-between align-items-center">
                            <span>{{$candidate->name}}</span>
                                <div data-toggle="tooltip" data-placement="bottom" title="Number of votes for this motion"><span class="badge badge-primary badge-pill">{{$ballot->votes->count()}}</span></div>
                            </div>
                        @endforeach

                        @if($token->id === $ballot->token_id || $ballot->write_in)<create-candidate :campaign="{{$campaign}}" :ballot="{{$ballot}}" csrf="{{  csrf_token() }}"></create-candidate>@endif

                       <button class="col-4 btn btn-success my-3 mx-auto" onclick="window.location='/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}/toggle_polls_open'">Open the Vote!</button>
                    </div>

                    @endif
                </div>


            </div>


            <div class="col-lg-4 col-12 h-100 d-sm-none d-md-block ">

                @if($ballot->polls_open)
                    <div class="p-3 bg-white rounded shadow-sm col-12 mb-3">
                        <h5>Share</h5>
                        <h4>Code: {{$ballot->slug}}</h4>
                        <hr />
                        <div class="col px-0">


                            <a class="btn btn-primary m-1" id="smsShare">
                                <span class="fas fa-mobile-alt"></span> Text
                            </a>

                            <a class="btn btn-primary m-1" href="mailto:?subject={{$campaign->name}}body=Cast your vote at: http://friendly.vote/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}">
                                <span class="fas fa-envelope"></span> Email
                            </a>

                            <a class="btn btn-primary m-1" href="#">
                                <span class="fab fa-twitter"></span> Tweet
                            </a>

                            <a class="btn btn-primary m-1" href="#">
                                <span class="fab fa-facebook-messenger"></span> Messenger
                            </a>

                            <a class="btn btn-primary m-1" href="#">
                                <span class="fab fa-whatsapp"></span> WhatsApp
                            </a>

                        </div>
                    </div>
                @endif

                <div class="p-3 bg-white rounded shadow-sm col-12 mb-3">
                    <h5>Options</h5>
                    <hr />
                    <div class="col px-0">
                        @if(!$ballot->complete)
                            <div class="my-2 row">
                                <div class="col-7">Voting is currently</div>

                                <div class="col-5 text-left">
                                    @if($token->id === $ballot->token_id) <a href="/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}/toggle_polls_open"> @endif
                                        <span class="badge @if($ballot->polls_open) badge-success @else badge-warning @endif">
                                    @if($ballot->polls_open) open @else paused @endif
                                </span>
                                        @if($token->id === $ballot->token_id)</a>@endif
                                </div>
                            </div>
                            <div class="my-2 row">
                                <div class="col-7">
                                    Write-in candidates
                                </div>
                                <div class="col-5 text-left">
                                    @if($token->id === $ballot->token_id) <a href="/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}/toggle_write_in"> @endif
                                        <span class="badge @if($ballot->write_in) badge-success @else badge-danger @endif">
                                    @if($ballot->write_in) allowed @else disallowed @endif
                                </span>
                                        @if($token->id === $ballot->token_id)</a>@endif
                                </div>
                            </div>
                        @elseif($ballot->complete)
                            <div class="alert-success alert"><span class="far fa-check-circle mr-1"></span> Voting Closed</div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
    {{--<script type="text/javascript">--}}

        {{--let setupSMS = function() {--}}
            {{--let a = document.getElementById("smsShare");--}}

            {{--let MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera;--}}

            {{--if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) {--}}
                {{--a.href = "sms:&body=" + encodeURI("Vote at: http://friendly.vote/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}");--}}
            {{--} else if (MobileUserAgent.match(/Android/i)) {--}}
                {{--a.href = "sms:?body=" + encodeURI("Vote at: http://friendly.vote/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}");--}}
            {{--} else {--}}
                {{--a.href = "sms:?body=" + encodeURI("Vote at: http://friendly.vote/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}");--}}
            {{--}--}}
        {{--};--}}
        {{--setupSMS();--}}
    {{--</script>--}}
@endsection
