@extends('common.template')

@section('content')

    <div class="w-100 container mt-3">

        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a> </li>
            <li class="breadcrumb-item">Sessions</li>
        </ul>

        <div class="row col-12 justify-content-between">
            <div class="d-sm-none d-md-block col-md-3">
                <h5>Options</h5>
                <div class="col">
                    <div class="my-2">
                        <a href="#">Create New Session</a>
                    </div>
                    <div class="my-2">
                        <a href="#">Settings</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-9 col-sm-12">
                <h5 class="mb-2">Sessions</h5>

                <div class="list-group">
                    @foreach($campaigns as $campaign)
                    <a href="/campaign/{{$campaign->slug}}" class="list-group-item d-flex justify-content-between align-items-center">
                        <span>
                            {{$campaign->name}}
                            <span class="ml-1 text-muted text-s12">({{$campaign->slug}})</span>
                        </span>
                        <div data-toggle="tooltip" data-placement="bottom" title="Number of votes in session"><span class="badge badge-primary badge-pill">{{$campaign->ballots->count()}}</span></div>
                    </a>
                    @endforeach

                </div>
            </div>
        </div>
    </div>

@endsection
