@extends('common.template')

@section('content')

    <div class="w-100 container mt-3">
        <h3>{{$campaign->name}} - Session</h3>

        <ul class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Home</a> </li>
            <li class="breadcrumb-item"><a href="/campaign">Session</a></li>
            <li class="breadcrumb-item">{{$campaign->name}}</li>
        </ul>

        <div class="row col-12 justify-content-between">
            <div class="d-sm-none d-md-block col-md-4 h-100 pr-4">
                <div class="p-3 bg-white rounded shadow-sm col-12">
                    <h5>Options</h5>
                    <hr class="mb-2 mt-1" />
                    <div class="col">
                        <div class="my-2">
                            <a href="#">Rename Session</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-sm-12 p-3 bg-white rounded shadow-sm">
                <h5 class="mb-2">Motions</h5>
                <hr class="mb-2 mt-1" />

                <p class="text-muted">List of votes that have been called for this event.</p>

                <div class="list-group">
                    @foreach($campaign->ballots as $ballot)
                    <a href="/campaign/{{$campaign->slug}}/ballot/{{$ballot->slug}}" class="list-group-item d-flex justify-content-between align-items-center">
                        <span>
                            {{$ballot->name}}
                            <span class="ml-1 text-muted text-s12">({{voteTypeToText($ballot->type)}})</span>
                        </span>
                        <div>
                            @if($ballot->complete)<span class="text-success far fa-check-circle text-s18" data-toggle="tooltip" data-placement="left" title="Voting has been completed"></span>@endif
                            @if(!$ballot->complete)<span data-toggle="tooltip" data-placement="bottom" title="Number of votes for this motion"><span class="badge badge-primary badge-pill">{{$ballot->votes->count()}}</span></span>@endif
                        </div>
                    </a>
                    @endforeach
                    <create-ballot :campaign="{{$campaign}}" csrf="{{  csrf_token() }}"></create-ballot>
                </div>
            </div>
        </div>
    </div>

@endsection
