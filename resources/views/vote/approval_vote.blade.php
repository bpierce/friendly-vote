<div class="column ballot simple">

    @if(!$ballot->complete)<approval-ballot :campaign="{{$campaign}}" :ballot="{{$ballot}}" :candidates="{{$ballot->candidates}}" :token="{{$token}}"></approval-ballot>@endif

    @if($ballot->complete)<simple-ballot-results :campaign="{{$campaign}}" :ballot="{{$ballot}}" :candidates="{{$ballot->candidates}}" :token="{{$token}}" :results="{{$results}}"></simple-ballot-results>@endif

</div>
