<div class="column ballot simple">

    @if(!$ballot->complete)<simple-ballot :campaign="{{$campaign}}" :ballot="{{$ballot}}" :candidates="{{$ballot->candidates}}" :token="{{$token}}"></simple-ballot>@endif

    @if($ballot->complete)<simple-ballot-results :campaign="{{$campaign}}" :ballot="{{$ballot}}" :candidates="{{$ballot->candidates}}" :token="{{$token}}" :results="{{$results}}"></simple-ballot-results>@endif

</div>
