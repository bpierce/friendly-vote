
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('apexcharts');

window.Vue = require('vue');

import _ from 'lodash';

Object.defineProperty(Vue.prototype, '$_', {value: _});

console.log('Vue super object', Vue.$_);

//Vue.use(VueLodash, options); // options is optional

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('create-ballot', require('./components/create-ballot'));
Vue.component('create-candidate', require('./components/create-candidate'));

Vue.component('simple-ballot', require('./components/simple-ballot'));
Vue.component('simple-ballot-results', require('./components/simple-ballot-results'));

Vue.component('approval-ballot', require('./components/approval-ballot'));

const app = new Vue({
    el: '#fv-app'
});


$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
