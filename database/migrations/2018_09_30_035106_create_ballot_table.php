<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBallotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ballots', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('campaign_id')->unsigned();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');

            $table->integer('token_id')->unsigned();
            $table->foreign('token_id')->references('id')->on('tokens');

            $table->string('slug')->unique();
            $table->string('name');

            $table->string('type');
            $table->jsonb('type_settings');

            $table->boolean('polls_open')->default(true)->comment('Allow votes to be cast');
            $table->boolean('write_in')->default(true)->comment('Allow users to add candidates to the ballot');

            $table->boolean('complete')->default(true)->comment('Voting has been completed');

            $table->timestamps();
        });


        Schema::create('candidates', function(Blueprint $table) {

            $table->increments('id');

            $table->integer('ballot_id')->unsigned();
            $table->foreign('ballot_id')->references('id')->on('ballots')->onDelete('cascade');

            $table->integer('token_id')->unsigned();
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('cascade');

            $table->uuid('uuid');
            $table->string( 'name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ballots');
        Schema::dropIfExists('candidates');
    }
}
