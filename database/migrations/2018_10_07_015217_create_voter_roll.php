<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoterRoll extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voter_rolls', function(Blueprint $table) {
            $table->increments('id');

            $table->integer('ballot_id')->unsigned();
            $table->foreign('ballot_id')->references('id')->on('ballots')->onDelete('cascade');

            $table->integer('token_id')->unsigned();
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('cascade');

            $table->integer('time_at_poll');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('voter_rolls');
    }
}
