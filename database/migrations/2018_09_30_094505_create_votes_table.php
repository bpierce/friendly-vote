<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->unique();

            $table->integer('ballot_id')->unsigned();
            $table->foreign('ballot_id')->references('id')->on('ballots')->onDelete('cascade');

            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')->references('id')->on('candidates')->onDelete('cascade');

            $table->integer('token_id')->unsigned();
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('cascade');

            $table->string('type')->nullable();
            $table->string('value')->nullable();
            $table->integer('ordinal')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
        Schema::dropIfExists('candidates');
    }
}
