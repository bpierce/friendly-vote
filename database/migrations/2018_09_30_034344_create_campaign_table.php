<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        /**
         * User/Token tracking table
         */
        Schema::create('tokens', function(Blueprint $table) {

            $table->increments('id');
            $table->uuid('uuid')->unique();
            $table->string('name')->nullable();
            $table->timestamps();

        });


        /**
         * Campaign tracking table
         */
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug',4)->unique();
            $table->string('name', 55);
            $table->dateTime('day')->nullable();

            $table->integer('token_id')->unsigned();
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
        Schema::dropIfExists('tokens');
    }
}
