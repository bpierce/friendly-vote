<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'Entry@index');

Route::post('/jump', 'Entry@findBallot');

Route::post('/campaign', 'CampaignsController@store');

Route::get('/campaign', 'CampaignsController@index');

Route::get('/campaign/{campaign}', 'CampaignsController@getCampaign');

Route::post('/campaign/{campaign}/ballot', 'BallotController@store');

Route::get('/campaign/{campaign}/ballot/{ballot}', 'BallotController@getBallot');

Route::get('/api/campaign/{campaign}/ballot/{ballot}', 'BallotController@jsonGetBallot');

Route::get('/campaign/{campaign}/ballot/{ballot}/toggle_polls_open', 'BallotController@togglePollsOpen');

Route::get('/campaign/{campaign}/ballot/{ballot}/toggle_write_in', 'BallotController@toggleWriteIn');

Route::post('/campaign/{campaign}/ballot/{ballot}/candidate', 'BallotController@createCandidate');

Route::post('/api/campaign/{campaign}/ballot/{ballot}/candidate', 'BallotController@apiCreateCandidate');

Route::post('/api/campaign/{campaign}/ballot/{ballot}/vote', 'BallotController@storeVote');

Route::post('/campaign/{campaign}/ballot/{ballot}/vote', 'BallotController@storeVote');

Route::get('/campaign/{campaign}/ballot/{ballot}/tally', 'BallotController@tallyBallot');

